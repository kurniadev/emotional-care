<?php include '../main_view/head.php'; ?>

<body>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php include '../main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area1" id="welcome">
        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                        <h1>
                            <strong>Selamat Datang</strong>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="container-fluid">
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-md-4 margin-bottom-100">
                <form class="form-container" method="post" action="../../function/cekRegister.php">
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="first_name" placeholder="Masukkan Nama Depan">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="last_name" placeholder="Masukkan Nama Belakang">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Password</label>
                        <input type="text" class="form-control" id="password" name="password" placeholder="Masukkan password" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Umur</label>
                        <input type="number" class="form-control" id="age" name="age" placeholder="Masukkan Umur" required>
                    </div>
                    <div class="form-group">
                        <label for="gender">Jenis Kelamin</label>
                        <select class="form-control" name="gender" required>
                        <option value="" selected>Pilih Jenis Kelamin</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kategori</label>
                        <select class="form-control" name="category">
                            <option value="" selected>Pilih Kategori</option>
                            <option value="Umum">Umum</option>
                            <option value="Panti">Panti</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Wilayah</label>
                        <select class="form-control" name="region">
                            <option value="" selected>Pilih Wilayah</option>
                            <option value="Surabaya">Surabaya</option>
                            <option value="Sidoarjo">Sidoarjo</option>
                            <option value="Lumajang">Lumajang</option>
                        </select>
                    </div>
                    <button type="submit" name="submit" value="Next" class="btn btn-success btn-block btn-lg">Lanjutkan</button>
                </form>
            </section>
        </section>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <?php include '../main_view/footer.php'; ?>