<?php
include '../../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../../login.php");
} else {
    $tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
    $peg    = mysqli_fetch_array($tampilPeg);
    $getGuest = mysqli_query($koneksi, "SELECT * FROM guest WHERE id_user='" . $peg['id_users'] . "'");
    $valueGuest = mysqli_fetch_array($getGuest);
}
if (isset($_POST['submit'])) {
    $name1 = isset($_POST['name1']) ? $_POST['name1'] : null;
    $name2 = isset($_POST['name2']) ? $_POST['name2'] : null;
    $name3 = isset($_POST['name3']) ? $_POST['name3'] : null;
    $name4 = isset($_POST['name4']) ? $_POST['name4'] : null;
    $name5 = isset($_POST['name5']) ? $_POST['name5'] : null;
    $name6 = isset($_POST['name6']) ? $_POST['name6'] : null;
    $name7 = isset($_POST['name7']) ? $_POST['name7'] : null;
    $name8 = isset($_POST['name8']) ? $_POST['name8'] : null;
    $name9 = isset($_POST['name9']) ? $_POST['name9'] : null;
    $name10 = isset($_POST['name10']) ? $_POST['name10'] : null;
    $name11 = isset($_POST['name11']) ? $_POST['name11'] : null;
    $name12 = isset($_POST['name12']) ? $_POST['name12'] : null;
    $name13 = isset($_POST['name13']) ? $_POST['name13'] : null;
    $name14 = isset($_POST['name14']) ? $_POST['name14'] : null;
    $name15 = isset($_POST['name15']) ? $_POST['name15'] : null;
    $name16 = isset($_POST['name16']) ? $_POST['name16'] : null;
    $name17 = isset($_POST['name17']) ? $_POST['name17'] : null;
    $name18 = isset($_POST['name18']) ? $_POST['name18'] : null;
    $name19 = isset($_POST['name19']) ? $_POST['name19'] : null;
    $name20 = isset($_POST['name20']) ? $_POST['name20'] : null;
    $name21 = isset($_POST['name21']) ? $_POST['name21'] : null;
    $name22 = isset($_POST['name22']) ? $_POST['name22'] : null;
    $name23 = isset($_POST['name23']) ? $_POST['name23'] : null;
    $name24 = isset($_POST['name24']) ? $_POST['name24'] : null;
    $name25 = isset($_POST['name25']) ? $_POST['name25'] : null;

    $total1 = $_POST['total1'];
    $hasil1 = $_POST['hasil1'];
    $total2 = $_POST['total2'];
    $hasil2 = $_POST['hasil2'];
    $total3 = $_POST['total3'];
    $hasil3 = $_POST['hasil3'];
    $total4 = $_POST['total4'];
    $hasil4 = $_POST['hasil4'];

    if ($name21 === null || $name22 === null || $name23 === null || $name24 === null || $name25 === null) {
        echo "Please fill in all fields.";
    } else {
        $total5 = $name21 + $name22 + $name23 + $name24 + $name25;
        if ($total5 <= 3) {
            $hasil5 = 'Normal';
        } else if ($total5 == 3) {
            $hasil5 = 'Borderline';
        } else {
            $hasil5 = 'Abnormal';
        }
    }
    $totalakhir = $total1 + $total2 + $total3 + $total4 + $total5;
    if ($totalakhir <= 14) {
        $hasilakhir = 'Normal';
        $results = '0';
    } else if ($totalakhir >= 14 & $totalakhir <= 16) {
        $hasilakhir = 'Borderline';
        $results = '1';
    } else {
        $hasilakhir = 'Abnormal';
        $results = '2';
    }


    $datenow = date_create()->format('Y-m-d H:i:s');
    $user_id = $_SESSION['id_users'];
    $query_guest = "INSERT INTO evaluation (id_user, perilaku_prososial, hiperaktivitas, masalah_perilaku, gejala_emosional, masalah_teman_sebaya, value_1, value_2, value_3, value_4, value_5, result, type, date)
    VALUES ('$user_id', '$total1', '$total2', '$total3', '$total4', '$total5', '$hasil1', '$hasil2', '$hasil3', '$hasil4', '$hasil5', '$results', '{$valueGuest['type']}', '$datenow')";


    $result_guest = mysqli_query($koneksi, $query_guest);
}

?>

<?php include '../main_view/head.php'; ?>

<body>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php include '../main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area2" id="welcome">
    </div>
    <!-- ***** Welcome Area End ***** -->

    <div class="container">

        <div class="container-fluid h-custom">
            <div class="row d-flex justify-content-center align-items-center h-100 margin-bottom-100">
                <div class="col-md-8 col-lg-8 col-xl-6">

                    <?php
                    if ($hasilakhir == 'Normal') {
                        echo ' <img src="../../assets/images/normal.png" alt="-" width="100%">';
                    } else if ($hasilakhir == 'Borderline') {
                        echo ' <img src="../../assets/images/borderline.png" alt="-" width="100%">';
                    } else {
                        echo ' <img src="../../assets/images/abnormal.png" alt="-" width="100%">';
                    }
                    ?>

                    <div class="sub-text text-center">
                        <h2>Kondisi anda adalah : <?php echo $hasilakhir; ?></h2><br>
                    </div>
                    <form method="post" action="detail-result.php">
                        <?php
                        $inputs = [
                            'name1', 'name2', 'name3', 'name4', 'name5',
                            'name6', 'name7', 'name8', 'name9', 'name10',
                            'name11', 'name12', 'name13', 'name14', 'name15',
                            'name16', 'name17', 'name18', 'name19',
                            'name21', 'name22', 'name23', 'name24', 'name25'
                        ];

                        foreach ($inputs as $input) {
                            echo "<input name=\"$input\" value=\"" . $$input . "\" hidden />\n";
                        }

                        $totals = [
                            'total1', 'total2', 'total3', 'total4', 'total5'
                        ];

                        $hasils = [
                            'hasil1', 'hasil2', 'hasil3', 'hasil4', 'hasil5'
                        ];

                        foreach ($totals as $total) {
                            echo "<input name=\"$total\" value=\"" . $$total . "\" hidden />\n";
                        }

                        foreach ($hasils as $hasil) {
                            echo "<input name=\"$hasil\" value=\"" . $$hasil . "\" hidden />\n";
                        }
                        ?>
                        <div class="text-center text-lg-start mt-4">
                            <button type="submit" name="submit" class="btn btn-success btn-block btn-lg margin-top-30">Lihat Detail</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <?php include '../main_view/footer.php'; ?>