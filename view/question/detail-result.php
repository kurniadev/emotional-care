<?php
include '../../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../../login.php");
} else {
    $tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
    $peg    = mysqli_fetch_array($tampilPeg);

    $getGuest = mysqli_query($koneksi, "SELECT * FROM guest WHERE id_user='" . $peg['id_users'] . "'");
    $valueGuest = mysqli_fetch_array($getGuest);
}
if (isset($_POST['submit'])) {
    $name1 = isset($_POST['name1']) ? $_POST['name1'] : null;
    $name2 = isset($_POST['name2']) ? $_POST['name2'] : null;
    $name3 = isset($_POST['name3']) ? $_POST['name3'] : null;
    $name4 = isset($_POST['name4']) ? $_POST['name4'] : null;
    $name5 = isset($_POST['name5']) ? $_POST['name5'] : null;
    $name6 = isset($_POST['name6']) ? $_POST['name6'] : null;
    $name7 = isset($_POST['name7']) ? $_POST['name7'] : null;
    $name8 = isset($_POST['name8']) ? $_POST['name8'] : null;
    $name9 = isset($_POST['name9']) ? $_POST['name9'] : null;
    $name10 = isset($_POST['name10']) ? $_POST['name10'] : null;
    $name11 = isset($_POST['name11']) ? $_POST['name11'] : null;
    $name12 = isset($_POST['name12']) ? $_POST['name12'] : null;
    $name13 = isset($_POST['name13']) ? $_POST['name13'] : null;
    $name14 = isset($_POST['name14']) ? $_POST['name14'] : null;
    $name15 = isset($_POST['name15']) ? $_POST['name15'] : null;
    $name16 = isset($_POST['name16']) ? $_POST['name16'] : null;
    $name17 = isset($_POST['name17']) ? $_POST['name17'] : null;
    $name18 = isset($_POST['name18']) ? $_POST['name18'] : null;
    $name19 = isset($_POST['name19']) ? $_POST['name19'] : null;
    $name20 = isset($_POST['name20']) ? $_POST['name20'] : null;
    $name21 = isset($_POST['name21']) ? $_POST['name21'] : null;
    $name22 = isset($_POST['name22']) ? $_POST['name22'] : null;
    $name23 = isset($_POST['name23']) ? $_POST['name23'] : null;
    $name24 = isset($_POST['name24']) ? $_POST['name24'] : null;
    $name25 = isset($_POST['name25']) ? $_POST['name25'] : null;


    $total1 = $_POST['total1'];
    $hasil1 = $_POST['hasil1'];
    $total2 = $_POST['total2'];
    $hasil2 = $_POST['hasil2'];
    $total3 = $_POST['total3'];
    $hasil3 = $_POST['hasil3'];
    $total4 = $_POST['total4'];
    $hasil4 = $_POST['hasil4'];
    $total5 = $_POST['total5'];
    $hasil5 = $_POST['hasil5'];

    if ($name21 === null || $name22 === null || $name23 === null || $name24 === null || $name25 === null) {
        echo "Please fill in all fields.";
    } else {
        $total5 = $name21 + $name22 + $name23 + $name24 + $name25;
        if ($total5 <= 3) {
            $hasil5 = 'Normal';
        } else if ($total5 == 3) {
            $hasil5 = 'Borderline';
        } else {
            $hasil5 = 'Abnormal';
        }
    }
    $totalakhir = $total1 + $total2 + $total3 + $total4 + $total5;
    if ($totalakhir <= 14) {
        $hasilakhir = 'Normal';
    } else if ($totalakhir >= 14 & $totalakhir <= 16) {
        $hasilakhir = 'Borderline';
    } else {
        $hasilakhir = 'Abnormal';
    }
}

?>

<?php include '../main_view/head.php'; ?>

<body>
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php include '../main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <div class="welcome-area2" id="welcome"></div>
    <div class="container">
        <center>
            <div class="sub-text">
                <h2><b>SKORING EMOSI DAN PERILAKU PADA ANAK</h2><br>
                <h2>(STRENGTH AND DIIFICULTIES QUESTIONNAIRE-SDQ</b>)</h2><br>
                <p>11-18 tahun</p>
            </div>

        </center>
        <table class="table table-striped table-bordered margin-top-30">
            <tbody>
                <tr>
                    <th scope="row">Nama Lengkap</th>
                    <td><?php echo $valueGuest['first_name'] . ' ' . $valueGuest['last_name']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Jenis Kelamin</th>
                    <td><?php echo $valueGuest['gender']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Umur</th>
                    <td><?php echo $valueGuest['age']; ?> Tahun</td>
                </tr>
                <tr>
                    <th scope="row">Kategori</th>
                    <td><?php echo $valueGuest['type']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Kondisi</th>
                    <td><?php echo $hasilakhir; ?></td>
                </tr>
            </tbody>
        </table><br>
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="table-dark">
                    <th>Penilaian</th>
                    <th>Skor</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <!-- <th scope="row">1</th> -->
                    <td>Perilaku Prososial</td>
                    <td><?php echo $total1; ?> </td>
                    <td><?php echo $hasil1; ?></td>
                </tr>
                <tr>
                    <!-- <th scope="row">2</th> -->
                    <td>Hiperaktivitas</td>
                    <td><?php echo $total2; ?> </td>
                    <td> <?php echo $hasil2; ?></td>
                </tr>
                <tr>
                    <!-- <th scope="row">3</th> -->
                    <td>Masalah Perilaku</td>
                    <td><?php echo $total3; ?> </td>
                    <td> <?php echo $hasil3; ?></td>
                </tr>
                <tr>
                    <!-- <th scope="row">4</th> -->
                    <td>Gejala Emosional</td>
                    <td><?php echo $total4; ?> </td>
                    <td><?php echo $hasil5; ?></td>
                </tr>
                <tr>
                    <!-- <th scope="row">5</th> -->
                    <td>Masalah Teman Sebaya</td>
                    <td><?php echo $total5; ?> </td>
                    <td><?php echo $hasil5; ?></td>
                </tr>
            </tbody>
        </table>
        <center>
            <div class="sub-text">
                <h1>
                    <?php echo $name1; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name2; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name3; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name4; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name5; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total1; ?><br> dengan hasil akhir <?php echo $hasil1; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name6; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name7; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name8; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name9; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name10; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total2; ?><br> dengan hasil akhir <?php echo $hasil2; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name11; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name12; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name13; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name14; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name15; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total3; ?><br> dengan hasil akhir <?php echo $hasil3; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name16; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name17; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name18; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name19; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name20; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total4; ?><br> dengan hasil akhir <?php echo $hasil4; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name21; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name22; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name23; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name24; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name25; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total5; ?><br> dengan hasil akhir <?php echo $hasil5; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Hasil Akhir
                </h1>
            </div>

            <div class="sub-text">
                <h1>
                    Total : <?php echo $totalakhir; ?><br> dengan hasil akhir <?php echo $hasilakhir; ?>
                </h1>
            </div>
        </center>
    </div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <?php include '../main_view/footer.php'; ?>