<?php
include '../../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
	header("Location: ../../login.php");
}else{
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
}
?>

<?php include '../main_view/head.php'; ?>

<body>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
            <div class="col-12">
                    <?php include '../main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area1" id="welcome">
        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                        <h1>
                            <strong>Perilaku Prososial</strong>
                        </h1>
                        <h2>Copyright &copy; Develop By Reza Kurnia</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->

    <div class="container">
        <center>
            <form method="post" action="hiperaktivitas.php" onsubmit="return validateForm()">
            <input name="first_name" value="<?php echo $first_name; ?>" hidden />
            <input name="last_name" value="<?php echo $last_name; ?>" hidden />
            <input name="age" value="<?php echo $age; ?>" hidden />
            <input name="gender" value="<?php echo $gender; ?>" hidden />
            <input name="category" value="<?php echo $category; ?>" hidden />
                <?php
                $query = "SELECT * FROM question WHERE category_id = 1 ";
                $result = mysqli_query($koneksi, $query);
                if (!$result) {
                    die("Query Error: " . mysqli_errno($koneksi) .
                        " - " . mysqli_error($koneksi));
                }
                $no = 1; //variabel untuk membuat nomor urut
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <div class="sub-text">
                        <h1>
                        <?php echo $row['name_question']; ?>
                        </h1>
                    </div>
                    <div class="wrapper">
                        <label class="radiobuttoncheck">
                            Benar
                            <input type="radio" value="2" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>
                        <label class="radiobuttoncheck">
                            Agak Benar
                            <input type="radio" value="1" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>

                        <label class="radiobuttoncheck">
                            Tidak Benar
                            <input type="radio" value="0" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>
                    </div>
                <?php
                    $no++; //untuk nomor urut terus bertambah 1
                }
                ?><br>
                  <button type="submit" name="submit" value="Next" class="btn btn-success btn-block btn-lg margin-bottom-60">Lanjutkan</button>
            </form>
        </center>
    </div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <script>
        function validateForm() {
            var name1 = document.getElementsByName('name1');
            var name2 = document.getElementsByName('name2');
            var name3 = document.getElementsByName('name3');
            var name4 = document.getElementsByName('name4');
            var name5 = document.getElementsByName('name5');
            var unanswered = false;

            for (var i = 0; i < name1.length; i++) {
                if (name1[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 1");
                return false;
            }

            unanswered = false;

            for (var i = 0; i < name2.length; i++) {
                if (name2[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 2");
                return false;
            }

            unanswered = false;

            for (var i = 0; i < name3.length; i++) {
                if (name3[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 3");
                return false;
            }
            unanswered = false;

            for (var i = 0; i < name4.length; i++) {
                if (name4[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 4");
                return false;
            }
            unanswered = false;

            for (var i = 0; i < name5.length; i++) {
                if (name5[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 5");
                return false;
            }

            return true;
        }
        </script>
<?php include '../main_view/footer.php'; ?>