<?php
include '../../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
	header("Location: ../../login.php");
}else{
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
}
if (isset($_POST['submit'])) {
    $name1 = isset($_POST['name1']) ? $_POST['name1'] : null;
    $name2 = isset($_POST['name2']) ? $_POST['name2'] : null;
    $name3 = isset($_POST['name3']) ? $_POST['name3'] : null;
    $name4 = isset($_POST['name4']) ? $_POST['name4'] : null;
    $name5 = isset($_POST['name5']) ? $_POST['name5'] : null;
    $name6 = isset($_POST['name6']) ? $_POST['name6'] : null;
    $name7 = isset($_POST['name7']) ? $_POST['name7'] : null;
    $name8 = isset($_POST['name8']) ? $_POST['name8'] : null;
    $name9 = isset($_POST['name9']) ? $_POST['name9'] : null;
    $name10 = isset($_POST['name10']) ? $_POST['name10'] : null;
    $name11 = isset($_POST['name11']) ? $_POST['name11'] : null;
    $name12 = isset($_POST['name12']) ? $_POST['name12'] : null;
    $name13 = isset($_POST['name13']) ? $_POST['name13'] : null;
    $name14 = isset($_POST['name14']) ? $_POST['name14'] : null;
    $name15 = isset($_POST['name15']) ? $_POST['name15'] : null;

    $total1 = $_POST['total1'];
    $hasil1 = $_POST['hasil1'];
    $total2 = $_POST['total2'];
    $hasil2 = $_POST['hasil2'];

    if ($name11 === null || $name12 === null || $name13 === null || $name14 === null || $name15 === null) {
        echo "Please fill in all fields.";
    } else {
        $total3 = $name11 + $name12 + $name13 + $name14 + $name15;
        if ($total3 <= 3) {
            $hasil3 = 'Normal';
        } else if ($total3 == 3) {
            $hasil3 = 'Borderline';
        } else {
            $hasil3 = 'Abnormal';
        }
    }
}

?>

<?php include '../main_view/head.php'; ?>

<body>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
            <div class="col-12">
                    <?php include '../main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area1" id="welcome">
        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                        <h1>
                            <strong>Gejala Emosional</strong>
                        </h1>
                        <h2>Copyright &copy; Develop By Reza Kurnia</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->

    <div class="container">
    <center>
            <form method="post" action="masalah-teman-sebaya.php" onsubmit="return validateForm()">
            <input name="name1" value="<?php echo $name1; ?>" hidden />
            <input name="name2" value="<?php echo $name2; ?>" hidden />
            <input name="name3" value="<?php echo $name3; ?>" hidden />
            <input name="name4" value="<?php echo $name4; ?>" hidden />
            <input name="name5" value="<?php echo $name5; ?>" hidden />
            <input name="name6" value="<?php echo $name6; ?>" hidden />
            <input name="name7" value="<?php echo $name7; ?>" hidden />
            <input name="name8" value="<?php echo $name8; ?>" hidden />
            <input name="name9" value="<?php echo $name9; ?>" hidden />
            <input name="name10" value="<?php echo $name10; ?>" hidden />
            <input name="name11" value="<?php echo $name11; ?>" hidden />
            <input name="name12" value="<?php echo $name12; ?>" hidden />
            <input name="name13" value="<?php echo $name13; ?>" hidden />
            <input name="name14" value="<?php echo $name14; ?>" hidden />
            <input name="name15" value="<?php echo $name15; ?>" hidden />
            <input name="total1" value="<?php echo $total1; ?>" hidden />
            <input name="hasil1" value="<?php echo $hasil1; ?>" hidden />
            <input name="total2" value="<?php echo $total2; ?>" hidden />
            <input name="hasil2" value="<?php echo $hasil2; ?>" hidden />
            <input name="total3" value="<?php echo $total3; ?>" hidden />
            <input name="hasil3" value="<?php echo $hasil3; ?>" hidden />
                <?php
                $query = "SELECT * FROM question WHERE category_id = 4 ";
                $result = mysqli_query($koneksi, $query);
                if (!$result) {
                    die("Query Error: " . mysqli_errno($koneksi) .
                        " - " . mysqli_error($koneksi));
                }
                $no = 1; //variabel untuk membuat nomor urut
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <div class="sub-text">
                        <h1>
                        <?php echo $row['name_question']; ?>
                        </h1>
                    </div>
                    <div class="wrapper">
                    <label class="radiobuttoncheck">
                            Benar
                            <input type="radio" value="2" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>
                        <label class="radiobuttoncheck">
                            Agak Benar
                            <input type="radio" value="1" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>

                        <label class="radiobuttoncheck">
                            Tidak Benar
                            <input type="radio" value="0" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>
                    </div>
                <?php
                    $no++; //untuk nomor urut terus bertambah 1
                }
                ?><br>
                      <button type="submit" name="submit" value="Next" class="btn btn-success btn-block btn-lg margin-bottom-60">Lanjutkan</button>
            </form>
        </center>
        <center>
            <div class="sub-text">
                <h1>
                    <?php echo $name1; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name2; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name3; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name4; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name5; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total1; ?><br> dengan hasil akhir <?php echo $hasil1; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name6; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name7; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name8; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name9; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name10; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total2; ?><br> dengan hasil akhir <?php echo $hasil2; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name11; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name12; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name13; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name14; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name15; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total3; ?><br> dengan hasil akhir <?php echo $hasil3; ?>
                </h1>
            </div>
        </center>
    </div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script>
        function validateForm() {
            var name16 = document.getElementsByName('name16');
            var name17 = document.getElementsByName('name17');
            var name18 = document.getElementsByName('name18');
            var name19 = document.getElementsByName('name19');
            var name20 = document.getElementsByName('name20');
            var unanswered = false;

            for (var i = 0; i < name16.length; i++) {
                if (name16[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 1");
                return false;
            }

            unanswered = false;

            for (var i = 0; i < name17.length; i++) {
                if (name17[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 2");
                return false;
            }

            unanswered = false;

            for (var i = 0; i < name18.length; i++) {
                if (name18[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 3");
                return false;
            }
            unanswered = false;

            for (var i = 0; i < name19.length; i++) {
                if (name19[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 4");
                return false;
            }
            unanswered = false;

            for (var i = 0; i < name20.length; i++) {
                if (name20[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 5");
                return false;
            }

            return true;
        }
    </script>

<?php include '../main_view/footer.php'; ?>