<?php
include '../../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../../login.php");
} else {
    $tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
    $peg    = mysqli_fetch_array($tampilPeg);
}
if (isset($_POST['submit'])) {
    $name1 = isset($_POST['name1']) ? $_POST['name1'] : null;
    $name2 = isset($_POST['name2']) ? $_POST['name2'] : null;
    $name3 = isset($_POST['name3']) ? $_POST['name3'] : null;
    $name4 = isset($_POST['name4']) ? $_POST['name4'] : null;
    $name5 = isset($_POST['name5']) ? $_POST['name5'] : null;
    $name6 = isset($_POST['name6']) ? $_POST['name6'] : null;
    $name7 = isset($_POST['name7']) ? $_POST['name7'] : null;
    $name8 = isset($_POST['name8']) ? $_POST['name8'] : null;
    $name9 = isset($_POST['name9']) ? $_POST['name9'] : null;
    $name10 = isset($_POST['name10']) ? $_POST['name10'] : null;
    $name11 = isset($_POST['name11']) ? $_POST['name11'] : null;
    $name12 = isset($_POST['name12']) ? $_POST['name12'] : null;
    $name13 = isset($_POST['name13']) ? $_POST['name13'] : null;
    $name14 = isset($_POST['name14']) ? $_POST['name14'] : null;
    $name15 = isset($_POST['name15']) ? $_POST['name15'] : null;
    $name16 = isset($_POST['name16']) ? $_POST['name16'] : null;
    $name17 = isset($_POST['name17']) ? $_POST['name17'] : null;
    $name18 = isset($_POST['name18']) ? $_POST['name18'] : null;
    $name19 = isset($_POST['name19']) ? $_POST['name19'] : null;
    $name20 = isset($_POST['name20']) ? $_POST['name20'] : null;

    $total1 = $_POST['total1'];
    $hasil1 = $_POST['hasil1'];
    $total2 = $_POST['total2'];
    $hasil2 = $_POST['hasil2'];
    $total3 = $_POST['total3'];
    $hasil3 = $_POST['hasil3'];

    if ($name16 === null || $name17 === null || $name18 === null || $name19 === null || $name20 === null) {
        echo "Please fill in all fields.";
    } else {
        $total4 = $name16 + $name17 + $name18 + $name19 + $name20;
        if ($total4 <= 4) {
            $hasil4 = 'Normal';
        } else if ($total4 == 4) {
            $hasil4 = 'Borderline';
        } else {
            $hasil4 = 'Abnormal';
        }
    }


}

?>

<?php include '../main_view/head.php'; ?>

<body>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php include '../main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area1" id="welcome">
        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                        <h1>
                            <strong>Masalah Teman Sebaya </strong>
                        </h1>
                        <h2>Copyright &copy; Develop By Reza Kurnia</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->

    <div class="container">
        <center>
            <form method="post" action="result.php" onsubmit="return validateForm()">
                <input name="name1" value="<?php echo $name1; ?>" hidden />
                <input name="name2" value="<?php echo $name2; ?>" hidden />
                <input name="name3" value="<?php echo $name3; ?>" hidden />
                <input name="name4" value="<?php echo $name4; ?>" hidden />
                <input name="name5" value="<?php echo $name5; ?>" hidden />
                <input name="name6" value="<?php echo $name6; ?>" hidden />
                <input name="name7" value="<?php echo $name7; ?>" hidden />
                <input name="name8" value="<?php echo $name8; ?>" hidden />
                <input name="name9" value="<?php echo $name9; ?>" hidden />
                <input name="name10" value="<?php echo $name10; ?>" hidden />
                <input name="name11" value="<?php echo $name11; ?>" hidden />
                <input name="name12" value="<?php echo $name12; ?>" hidden />
                <input name="name13" value="<?php echo $name13; ?>" hidden />
                <input name="name14" value="<?php echo $name14; ?>" hidden />
                <input name="name15" value="<?php echo $name15; ?>" hidden />
                <input name="name16" value="<?php echo $name16; ?>" hidden />
                <input name="name17" value="<?php echo $name17; ?>" hidden />
                <input name="name18" value="<?php echo $name18; ?>" hidden />
                <input name="name19" value="<?php echo $name19; ?>" hidden />
                <input name="name20" value="<?php echo $name20; ?>" hidden />
                <input name="total1" value="<?php echo $total1; ?>" hidden />
                <input name="hasil1" value="<?php echo $hasil1; ?>" hidden />
                <input name="total2" value="<?php echo $total2; ?>" hidden />
                <input name="hasil2" value="<?php echo $hasil2; ?>" hidden />
                <input name="total3" value="<?php echo $total3; ?>" hidden />
                <input name="hasil3" value="<?php echo $hasil3; ?>" hidden />
                <input name="total4" value="<?php echo $total4; ?>" hidden />
                <input name="hasil4" value="<?php echo $hasil4; ?>" hidden />

                <?php
                $query = "SELECT * FROM question WHERE category_id = 5 ";
                $result = mysqli_query($koneksi, $query);
                if (!$result) {
                    die("Query Error: " . mysqli_errno($koneksi) .
                        " - " . mysqli_error($koneksi));
                }
                $no = 1; //variabel untuk membuat nomor urut
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <div class="sub-text">
                        <h1>
                            <?php echo $row['name_question']; ?>
                        </h1>
                    </div>
                    <div class="wrapper">
                        <label class="radiobuttoncheck">
                            Benar
                            <input type="radio" value="2" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>
                        <label class="radiobuttoncheck">
                            Agak Benar
                            <input type="radio" value="1" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>

                        <label class="radiobuttoncheck">
                            Tidak Benar
                            <input type="radio" value="0" name="<?php echo $row['names']; ?>">
                            <span class="radiobutton_mark"></span>
                        </label>
                    </div>
                <?php
                    $no++; //untuk nomor urut terus bertambah 1
                }
                ?><br>

                <!-- <button type="submit" name="submit" value="Next" class="btn btn-success btn-block btn-lg margin-bottom-60">Selesai</button> -->
                <div class="open-modal">Lanjutkan</div>
                <div id="myModal" class="modal">
                <div class="modal-content">
                    <p>Apakah anda sudah yakin selesai?</p>
                    <div class="button-container">
                        <button type="button" class="cancel-button">Batal</button>
                        <button type="submit" name="submit" value="Next" class="continue-button">Selesai</button>
                    </div>
                </div>
            </div>
            </form>
        </center>
        <center>
            <div class="sub-text">
                <h1>
                    <?php echo $name1; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name2; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name3; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name4; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name5; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total1; ?><br> dengan hasil akhir <?php echo $hasil1; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name6; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name7; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name8; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name9; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name10; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total2; ?><br> dengan hasil akhir <?php echo $hasil2; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name11; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name12; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name13; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name14; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name15; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total3; ?><br> dengan hasil akhir <?php echo $hasil3; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name16; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name17; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name18; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name19; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    <?php echo $name20; ?>
                </h1>
            </div>
            <div class="sub-text">
                <h1>
                    Total : <?php echo $total4; ?><br> dengan hasil akhir <?php echo $hasil4; ?>
                </h1>
            </div>
        </center>
    </div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script>
        function validateForm() {
            var name21 = document.getElementsByName('name21');
            var name22 = document.getElementsByName('name22');
            var name23 = document.getElementsByName('name23');
            var name24 = document.getElementsByName('name24');
            var name25 = document.getElementsByName('name25');
            var unanswered = false;

            for (var i = 0; i < name21.length; i++) {
                if (name21[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 1");
                return false;
            }

            unanswered = false;

            for (var i = 0; i < name22.length; i++) {
                if (name22[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 2");
                return false;
            }

            unanswered = false;

            for (var i = 0; i < name23.length; i++) {
                if (name23[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 3");
                return false;
            }
            unanswered = false;

            for (var i = 0; i < name24.length; i++) {
                if (name24[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 4");
                return false;
            }
            unanswered = false;

            for (var i = 0; i < name25.length; i++) {
                if (name25[i].checked) {
                    unanswered = true;
                    break;
                }
            }

            if (!unanswered) {
                alert("Please select at 5");
                return false;
            }

            return true;
        }
    </script>

    <?php include '../main_view/footer.php'; ?>