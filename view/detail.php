<?php
include '../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../login.php");
} else {
    $tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
    $peg    = mysqli_fetch_array($tampilPeg);

    $getGuest = mysqli_query($koneksi, "SELECT * FROM guest WHERE id_user='" . $peg['id_users'] . "'");
    $valueGuest = mysqli_fetch_array($getGuest);
}
if (isset($_GET['id'])) {
    $id = ($_GET["id"]);

    $query = "SELECT * FROM evaluation WHERE id_evaluation='$id'";
    $result = mysqli_query($koneksi, $query);

    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    // mengambil data dari database
    $datadetail = mysqli_fetch_assoc($result);
    // apabila data tidak ada pada database maka akan dijalankan perintah ini
    if (!count($datadetail)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    // apabila tidak ada data GET id pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id.');window.location='index.php';</script>";
}

?>

<?php include 'main_view/head.php'; ?>

<body>
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php include 'main_view/navbar.php'; ?>
                </div>
            </div>
        </div>
    </header>
    <div class="welcome-area2" id="welcome">
    </div>

    <div class="container">
        <center>
            <div class="sub-text">
                <h2><b>DETAIL SKORING EMOSI DAN PERILAKU PADA ANAK</h2><br>
                <h2>(STRENGTH AND DIIFICULTIES QUESTIONNAIRE-SDQ</b>)</h2><br>
                <p>11-18 tahun</p>
            </div>

        </center>
        <table class="table table-striped table-bordered margin-top-30">
            <tbody>
                <tr>
                    <th scope="row">Nama Lengkap</th>
                    <td><?php echo $valueGuest['first_name'] . ' ' . $valueGuest['last_name']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Jenis Kelamin</th>
                    <td><?php echo $valueGuest['gender']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Umur</th>
                    <td><?php echo $valueGuest['age']; ?> Tahun</td>
                </tr>
                <tr>
                    <th scope="row">Kategori</th>
                    <td><?php echo $valueGuest['type']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Kondisi</th>
                    <td><?php
                            if ($datadetail['result'] == 0) {
                                echo 'Normal';
                            } else if ($datadetail['result'] == 1) {
                                echo 'Borderline';
                            } else {
                                echo 'Abnormal';
                            } ?></td>
                </tr>
            </tbody>
        </table><br>
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="table-dark">
                    <th>Penilaian</th>
                    <th>Skor</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <!-- <th scope="row">1</th> -->
                    <td>Perilaku Prososial</td>
                    <td><?php echo$datadetail['perilaku_prososial']; ?> </td>
                    <td><?php echo$datadetail['value_1']; ?> </td>
                </tr>
                <tr>
                    <!-- <th scope="row">2</th> -->
                    <td>Hiperaktivitas</td>
                    <td><?php echo$datadetail['hiperaktivitas']; ?> </td>
                    <td><?php echo$datadetail['value_2']; ?> </td>
                </tr>
                <tr>
                    <!-- <th scope="row">3</th> -->
                    <td>Masalah Perilaku</td>
                    <td><?php echo$datadetail['masalah_perilaku']; ?> </td>
                    <td><?php echo$datadetail['value_3']; ?> </td>
                </tr>
                <tr>
                    <!-- <th scope="row">4</th> -->
                    <td>Gejala Emosional</td>
                    <td><?php echo$datadetail['gejala_emosional']; ?> </td>
                    <td><?php echo$datadetail['value_4']; ?> </td>
                </tr>
                <tr>
                    <!-- <th scope="row">5</th> -->
                    <td>Masalah Teman Sebaya</td>
                    <td><?php echo$datadetail['masalah_teman_sebaya']; ?> </td>
                    <td><?php echo$datadetail['value_5']; ?> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <?php include 'main_view/footer.php'; ?>