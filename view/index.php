<?php
include '../config/koneksi.php';
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../../login.php");
} else {
    $tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
    $peg    = mysqli_fetch_array($tampilPeg);

    $getGuest = mysqli_query($koneksi, "SELECT * FROM guest WHERE id_user='" . $peg['id_users'] . "'");
    $valueGuest = mysqli_fetch_array($getGuest);
}
//Last date
$query = "SELECT * FROM evaluation WHERE id_user='" . $peg['id_users'] . "'";
$resultEvaluation = mysqli_query($koneksi, $query);
$rowCount = mysqli_num_rows($resultEvaluation);
if ($rowCount > 0) {
    mysqli_data_seek($resultEvaluation, $rowCount - 1);
    $rowLast = mysqli_fetch_assoc($resultEvaluation);
} else {
    $rowLast = 'Belum ada';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet" />

    <title>Andini</title>
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/css/styling2.css" />
    <script src="../assets/js/Chart.js"></script>
</head>

<body>
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <?php
                        if (isset($_SESSION['email'])) {
                            echo '
                                <a href="index.php" class="logo">
                                    <img src="../assets/images/logo.png" alt="" />
                                </a>
                                <ul class="nav">
                                    <li><a class="logouts"  href="../function/cekLogout.php">Logout</a></li>
                                </ul>';
                        } else {
                            echo '
                                <a href="../index.php" class="logo">
                                    <img src="../assets/images/logo.png" alt="" />
                                </a>';
                        }
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="welcome-area2" id="welcome">
    </div>
    <div class="container">
        <div class="sub-text">
            <h3><b>Dashboard</b></h3><br>
            <h4><b>Selamat Datang, <?php echo $valueGuest['first_name'] . ' ' . $valueGuest['last_name']; ?></b></h4><br>
        </div><br>
        <div class="row stat-cards">
            <div class="col-md-6 col-xl-6">
                <article class="stat-cards-item">
                    <div class="stat-cards-icon primary">
                        <i data-feather="bar-chart-2" aria-hidden="true"></i>
                    </div>
                    <div class="stat-cards-info">
                        <p class="stat-cards-info__title">Terakhir Test</p>
                        <p class="stat-cards-info__num"><?php
                                                        if ($rowCount == 0) {
                                                            echo  $rowLast;
                                                        } else {
                                                            echo date("d-m-Y", strtotime($rowLast['date']));
                                                        }
                                                        ?></p>
                    </div>
                </article>
            </div>
            <div class="col-md-6 col-xl-6">
                <article class="stat-cards-item">
                    <div class="stat-cards-icon warning">
                        <i data-feather="file" aria-hidden="true"></i>
                    </div>
                    <div class="stat-cards-info">
                        <p class="stat-cards-info__title">Total Test</p>
                        <p class="stat-cards-info__num"><?php echo $rowCount; ?></p>
                    </div>
                </article>
            </div>
        </div><br><br>
        <a href="question/perilaku-prososial.php" class="btn btn-success btn-block btn-lg margin-bottom-60"> <i class="bi bi-play-fill"></i>Test Sekarang</a>
        <h4><b>Riwayat</b></h4><br>
        <div class="container" style="border: 1px solid; border-radius: 5px;">
            <canvas id="linechart" width="100" height="30"></canvas>
        </div><br>
        <table class="table table-bordered" style="border: 1px solid #dee2e6; border-radius: 5px;">
            <thead>
                <tr style="background-color: #1c8157;">
                    <th style="width: 3%;">No.</th>
                    <th>Tanggal</th>
                    <th>Hasil</th>
                    <th style="width: 20%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $query = "SELECT COUNT(*) as total FROM evaluation WHERE id_user='" . $peg['id_users'] . "'";
                $result = mysqli_query($koneksi, $query);
                if (!$result) {
                    die("Query Error: " . mysqli_errno($koneksi) . " - " . mysqli_error($koneksi));
                }
                $row = mysqli_fetch_assoc($result);
                $total_records = $row['total'];
                $records_per_page = 2;
                $total_pages = ceil($total_records / $records_per_page);

                if (!isset($_GET['page'])) {
                    $page = 1;
                } else {
                    $page = $_GET['page'];
                }

                $offset = ($page - 1) * $records_per_page;

                $query = "SELECT * FROM evaluation WHERE id_user='" . $peg['id_users'] . "' LIMIT $offset, $records_per_page";
                $result = mysqli_query($koneksi, $query);
                if (!$result) {
                    die("Query Error: " . mysqli_errno($koneksi) . " - " . mysqli_error($koneksi));
                }
                $no = $offset + 1;
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo date("d-M-Y", strtotime($row['date'])); ?></td>
                        <td><?php
                            if ($row['result'] == 0) {
                                echo 'Normal';
                            } else if ($row['result'] == 1) {
                                echo 'Borderline';
                            } else {
                                echo 'Abnormal';
                            } ?></td>
                        <td>
                            <center>
                                <a href="detail.php?id=<?php echo $row['id_evaluation']; ?>" type="button" class="btn btn-primary btn-block" style="width: 100%; height: 35px;">Detail</a>
                            </center>
                        </td>
                    </tr>
                <?php
                    $no++;
                }
                ?>
            </tbody>
        </table>

        <div class="pagination">
            <?php if ($total_pages > 1) {
                $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                echo '<a href="?page=1">&laquo;</a>';
                for ($i = max(1, $current_page - 5); $i <= min($current_page + 5, $total_pages); $i++) {
                    echo '<a href="?page=' . $i . '"';
                    if ($i == $current_page) {
                        echo ' class="active"';
                    }
                    echo '>' . $i . '</a>';
                }
                echo '<a href="?page=' . $total_pages . '">&raquo;</a>';
            } ?>
        </div>

        <div class="expended"></div>
    </div>

    <!-- ***** Footer Start ***** -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">
                        Copyright &copy; 2023 Tugas Akhir Andini Rahmadanti
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <?php include 'main_view/footer.php'; ?>
    <script type="text/javascript">
        var ctx = document.getElementById("linechart").getContext("2d");
        var data = {
            labels: [
                <?php
                $query = "SELECT * FROM evaluation WHERE id_user='" . $peg['id_users'] . "'";
                $resultEvaluation = mysqli_query($koneksi, $query);
                while ($row = mysqli_fetch_assoc($resultEvaluation)) {
                    $formattedDate = date_format(date_create($row['date']), 'd-M-Y');
                    if ($formattedDate != '') {
                        echo "'" . $formattedDate . "',";
                    } else {
                        echo "0";
                    }
                }
                ?>
            ],
            datasets: [{
                label: "Hasil",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#007bff",
                borderColor: "#007bff",
                pointHoverBackgroundColor: "#007bff",
                pointHoverBorderColor: "#007bff",
                data: [
                    <?php
                    $query = "SELECT * FROM evaluation WHERE id_user='" . $peg['id_users'] . "'";
                    $resultEvaluation = mysqli_query($koneksi, $query);
                    while ($row = mysqli_fetch_assoc($resultEvaluation)) {
                        echo "'" . $row['result'] . "',";
                    }
                    ?>
                ]
            }, ]
        };

        var myBarChart = new Chart(ctx, {
            type: 'line',
            data: data,
            // options: {
            //     legend: {
            //         display: true
            //     },
            //     barValueSpacing: 20,
            //     scales: {
            //         yAxes: [{
            //             ticks: {
            //                 min: 0,
            //             }
            //         }],
            //         xAxes: [{
            //             gridLines: {
            //                 color: "rgba(0, 0, 0, 0)",
            //             }
            //         }]
            //     }
            // }
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            // max: 25,
                            min: 0,
                            stepSize: 1,
                            callback: function(value, values) {
                                switch (value) {
                                    case 0:
                                        return 'Normal';
                                    case 1:
                                        return 'Borderline';
                                    case 2:
                                        return 'Abnormal';
                                    default:
                                        return value;
                                }
                            }
                        }
                    }]
                }
            }
        });
    </script>