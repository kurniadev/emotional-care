<!-- jQuery -->
<script src="../../assets/js/jquery-2.1.0.min.js"></script>

<!-- Bootstrap -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Plugins -->
<script src="../../assets/js/scrollreveal.min.js"></script>
<!-- Global Init -->
<script src="../../assets/js/custom.js"></script>
<script>
 // Get the modal element
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementsByClassName("open-modal")[0];

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Get the buttons for continue and cancel
var continueBtn = document.getElementsByClassName("continue-button")[0];
var cancelBtn = document.getElementsByClassName("cancel-button")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
};

// When the user clicks on <span> (x) or the cancel button, close the modal
span.onclick = cancelBtn.onclick = function() {
  modal.style.display = "none";
};

// When the user clicks the continue button, perform the desired action and close the modal
continueBtn.onclick = function() {
  // Add your desired action here
  alert("Continue button clicked");
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

</script>
</body>

</html>