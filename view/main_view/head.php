<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet" />

    <title>Andini</title>
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="../../assets/css/font-awesome.css" />

    <link rel="stylesheet" href="../../assets/css/styling2.css" />
    <style>
        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
        }

        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            max-width: 400px;
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .open-modal {
            padding: 10px 20px;
            background-color: #33a474;
            color: white;
            border: none;
            cursor: pointer;
        }

        .open-modal:hover {
            background-color: #1c8157;
        }

        .button-container {
            margin-top: 20px;
            text-align: center;
        }

        .continue-button
        {
            padding: 10px 20px;
            margin-right: 10px;
            background-color: #4CAF50;
            color: white;
            border-radius: 18px;
            border: none;
            cursor: pointer;
        }
        .cancel-button {
            padding: 10px 20px;
            margin-right: 10px;
            background-color: #e53434;
            color: white;
            border-radius: 18px;
            border: none;
            cursor: pointer;
        }
    </style>
</head>