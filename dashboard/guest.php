<?php
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../login.php");
}
include '../config/koneksi.php';
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
$username = strtoupper($peg['username']);
$fstchar = mb_substr($username, 0, 1);
$sideName = basename($_SERVER['PHP_SELF'], ".php");
// echo "$sideName";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/svg/logo.svg" type="image/x-icon">
    <!-- Custom styles -->
    <link rel="stylesheet" href="assets/css/style.min.css">
</head>

<body>
    <div class="layer"></div>
    <!-- ! Body -->
    <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
    <div class="page-flex">
        <!-- ! Sidebar -->
        <?php include 'component/sidebar.php'; ?>
        <div class="main-wrapper">
            <!-- ! Main nav -->
            <?php include 'component/main-navbar.php'; ?>
            <!-- ! Main -->
            <main class="main users chart-page" id="skip-target">
                <div class="container">
                    <h2 class="main-title">Guest</h2>
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="users-table table-wrapper">
                                <table class="posts-table">
                                    <thead>
                                        <tr class="users-table-info">
                                            <th style="width : 60px">
                                                <label class="users-table ms-20">
                                                    No.
                                                </label>
                                            </th>
                                            <th>Gambar</th>
                                            <th>Nama Lengkap</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Umur</th>
                                            <th>Kategori</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
                                        $query = "SELECT * FROM guest ORDER BY created_at DESC ";
                                        $result = mysqli_query($koneksi, $query);

                                        //mengecek apakah ada error ketika menjalankan query
                                        if (!$result) {
                                            die("Query Error: " . mysqli_errno($koneksi) .
                                                " - " . mysqli_error($koneksi));
                                        }

                                        //buat perulangan untuk element tabel dari data mahasiswa
                                        $no = 1; //variabel untuk membuat nomor urut
                                        // hasil query akan disimpan dalam variabel $data dalam bentuk array
                                        // kemudian dicetak dengan perulangan while
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            
                                        ?>
                                            <tr>
                                                <td><?php echo $no; ?>.</td>
                                                <td>
                                                    <div class="pages-table-img">
                                                        <span class="<?= array_rand(array_flip(['random-user-img1', 'random-user-img2', 'random-user-img3', 'random-user-img4', 'random-user-img5'])) ?>">
                                                        <?php echo mb_substr($row['first_name'], 0, 1); ?> <?php echo mb_substr($row['last_name'], 0, 1); ?>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                <?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?>
                                                </td>
                                                <td><?php echo $row['gender']; ?></td>
                                                <td><?php echo $row['age']; ?></td>
                                                <td><?php echo $row['type']; ?></td>
                                                <td>
                                                    <span class="p-relative">
                                                        <button class="dropdown-btn transparent-btn" type="button" title="More info">
                                                            <div class="sr-only">Laki-laki</div>
                                                            <i data-feather="more-horizontal" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class="users-item-dropdown dropdown">
                                                            <li><a href="##">Edit</a></li>
                                                            <li><a href="##">Quick edit</a></li>
                                                            <li><a href="##">Trash</a></li>
                                                        </ul>
                                                    </span>
                                                </td>
                                            </tr>
                                        <?php
                                            $no++; //untuk nomor urut terus bertambah 1
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!-- ! Footer -->
            <footer class="footer">
                <div class="container footer--flex">
                    <div class="footer-start">
                        <p>2023</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- Chart library -->
    <script src="assets/plugins/chart.min.js"></script>
    <!-- Icons library -->
    <script src="assets/plugins/feather.min.js"></script>
    <!-- Custom scripts -->
    <script src="assets/js/script.js"></script>
</body>

</html>