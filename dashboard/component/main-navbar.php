<nav class="main-nav--bg">
    <div class="container main-nav">
        <div class="main-nav-start">
        </div>
        <div class="main-nav-end">
            <button class="sidebar-toggle transparent-btn" title="Menu" type="button">
                <span class="sr-only">Toggle menu</span>
                <span class="icon menu-toggle--gray" aria-hidden="true"></span>
            </button>
            <div class="nav-user-wrapper">
                <button href="##" class="nav-user-btn dropdown-btn" title="My profile" type="button">
                    <span class="sr-only">My profile</span>
                    <span class="nav-user-img">
                    <?php echo $fstchar; ?>
                    </span>
                </button>
                <ul class="users-item-dropdown nav-user-dropdown dropdown">
                    <li><a href="##">
                            <span>Hai <?php echo $peg['username']; ?> </span></li>
                    <li><a class="danger" href="../function/cekLogout.php">
                            <i data-feather="log-out" aria-hidden="true"></i>
                            <span>Log out</span>
                        </a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>