<!-- <aside class="sidebar">
	<div class="sidebar-start">
		<div class="sidebar-head">
			<a href="/dashboard/index.php" class="logo-wrapper" title="Home">
				<span class="sr-only">Home</span>
				<div class="logo-text">
					<span class="logo-title">Andini Skuy</span>
					<span class="logo-subtitle">Administrator</span>
				</div>

			</a>
			<button class="sidebar-toggle transparent-btn" title="Menu" type="button">
				<span class="sr-only">Toggle menu</span>
				<span class="icon menu-toggle" aria-hidden="true"></span>
			</button>
		</div>
		<div class="sidebar-body">
			<div class="sidebar-body">
				<ul class="sidebar-body-menu">
					<li>
						<a class="<?= ($sideName == 'index') ? 'active' : ''; ?>" href="index.php"><span class="icon home" aria-hidden="true"></span>Dashboard</a>
					</li>
					<li>
						<a class="<?= ($sideName == 'question') ? 'active' : ''; ?>" href="question.php"><span class="icon edit" aria-hidden="true"></span>Question</a>
					</li>
					<li>
						<a class="<?= ($sideName == 'users') ? 'active' : ''; ?>" href="users.php"><span class="icon user-3" aria-hidden="true"></span>Users</a>
					</li>
					<li>
						<a class="<?= ($sideName == 'guest') ? 'active' : ''; ?>" href="guest.php"><span class="icon message" aria-hidden="true"></span>Guest</a>
					</li>
				</ul>
			</div>
		</div>
</aside> -->

<aside class="sidebar">
	<div class="sidebar-start">
		<div class="sidebar-head">
			<a href="/dashboard/index.php" class="logo-wrapper" title="Home">
				<span class="sr-only">Home</span>
				<div class="logo-text">
					<span class="logo-title">Andini Skuy</span>
					<span class="logo-subtitle">Administrator</span>
				</div>

			</a>
			<button class="sidebar-toggle transparent-btn" title="Menu" type="button">
				<span class="sr-only">Toggle menu</span>
				<span class="icon menu-toggle" aria-hidden="true"></span>
			</button>
		</div>
		<div class="sidebar-body">
			<ul class="sidebar-body-menu">
				<ul class="sidebar-body-menu">
					<li>
						<a class="<?= ($sideName == 'index') ? 'active' : ''; ?>" href="index.php"><i class="fa-solid fa-house-chimney" style="font-size: 20px; margin-right: 5px; width: 30px; display: inline-block; text-align: center;"></i>Dashboard</a>
					</li>
					<li>
						<a class="<?= ($sideName == 'question') ? 'active' : ''; ?>" href="question.php"><i class="fa-solid fa-file" style="font-size: 20px; margin-right: 5px; width: 30px; display: inline-block; text-align: center;"></i>Question</a>
					</li>
					<li>
						<a class="<?= ($sideName == 'users') ? 'active' : ''; ?>" href="users.php"><i class="fa-solid fa-user" style="font-size: 20px; margin-right: 5px; width: 30px; display: inline-block; text-align: center;"></i>User</a>
					</li>
				</ul>
			</ul>
			<!-- <span class="system-menu__title">system</span> -->
			<ul class="sidebar-body-menu">
				<li>
					<a class="show-cat-btn" href="##">
						<i class="fa-solid fa-chart-pie" style="font-size: 20px; margin-right: 5px; width: 30px; display: inline-block; text-align: center;"></i>Data Analisis
						<span class="category__btn transparent-btn" title="Open list">
							<span class="sr-only">Open list</span>
							<span class="icon arrow-down" aria-hidden="true"></span>
						</span>
					</a>
					<ul class="cat-sub-menu <?php echo ($sideName == 'area-data') ? 'visible' : (($sideName == 'gender-data') ? 'visible' : (($sideName == 'category-data') ? 'visible' : (($sideName == 'condition-data') ? 'visible' : ''))); ?>">
						<li>
							<a class="<?= ($sideName == 'area-data') ? 'active' : ''; ?>" href="area-data.php">Data Wilayah</a>
						</li>
						<li>
							<a class="<?= ($sideName == 'gender-data') ? 'active' : ''; ?>" href="gender-data.php">Data Jenis Kelamin</a>
						</li>
						<li>
							<a class="<?= ($sideName == 'category-data') ? 'active' : ''; ?>" href="category-data.php">Data Kategori</a>
						</li>
						<li>
							<a class="<?= ($sideName == 'condition-data') ? 'active' : ''; ?>" href="condition-data.php">Data Kondisi</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</aside>