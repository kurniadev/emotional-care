<?php
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
	header("Location: ../login.php");
}
include '../config/koneksi.php';
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
$username = strtoupper($peg['username']);
$fstchar = mb_substr($username, 0, 1);
$sideName = "category-data";

// Get Count Sby
$querySby = "SELECT COUNT(*) as count FROM guest WHERE region = 'Sidoarjo'";
$resultSby = mysqli_query($koneksi, $querySby);
$rowSby = mysqli_fetch_assoc($resultSby);
$sbyCount = $rowSby['count'];

// Get Count Lmj
$queryLmj = "SELECT COUNT(*) as count FROM guest WHERE region = 'Lumajang'";
$resultLmj = mysqli_query($koneksi, $queryLmj);
$rowLmj = mysqli_fetch_assoc($resultLmj);
$lmjCount = $rowLmj['count'];

// Get Count Lmj
$querySda = "SELECT COUNT(*) as count FROM guest WHERE region = 'Sidoarjo'";
$resultSda = mysqli_query($koneksi, $querySda);
$rowSda = mysqli_fetch_assoc($resultSda);
$sdaCount = $rowSda['count'];

// Calculate percentage
$totalCount = $sbyCount + $lmjCount + $sdaCount;
$sbyPercentage = ($sbyCount / $totalCount) * 100;
$lmjPercentage = ($lmjCount / $totalCount) * 100;
$sdaPercentage = ($sdaCount / $totalCount) * 100;

$dataPoints = array(
	array("label" => "Surabaya", "y" => $sbyPercentage),
	array("label" => "Lumajang", "y" => $lmjPercentage),
	array("label" => "Sidoarjo", "y" => $sdaPercentage),
);

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard</title>
	<link rel="shortcut icon" href="assets/img/svg/logo.svg" type="image/x-icon">
	<link rel="stylesheet" href="assets/css/style.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
	<div class="layer"></div>
	<a class="skip-link sr-only" href="#skip-target">Skip to content</a>
	<div class="page-flex">
		<?php include 'component/sidebar.php'; ?>
		<div class="main-wrapper">
			<?php include 'component/main-navbar.php'; ?>
			<main class="main users chart-page" id="skip-target">
				<div class="container">
					<h2 class="main-title">Data Wilayah</h2>
					<div class="row stat-cards">
						<div class="col-md-4 col-xl-4">
							<article class="stat-cards-item">
								<div class="stat-cards-icon primary">
									<i class="fa-sharp fa-solid fa-location-dot" style="font-size: 30px; display: inline-block; text-align: center;"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num"><?php echo $sbyCount; ?> Orang</p>
									<p class="stat-cards-info__title">Total Wilayah Surabaya</p>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-xl-4">
							<article class="stat-cards-item">
								<div class="stat-cards-icon primary">
									<i class="fa-sharp fa-solid fa-location-dot" style="font-size: 30px; display: inline-block; text-align: center;"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num"><?php echo $sdaCount; ?> Orang</p>
									<p class="stat-cards-info__title">Total Wilayah Sidoarjo</p>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-xl-4">
							<article class="stat-cards-item">
								<div class="stat-cards-icon primary">
									<i class="fa-sharp fa-solid fa-location-dot" style="font-size: 30px; display: inline-block; text-align: center;"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num"><?php echo $lmjCount; ?> Orang</p>
									<p class="stat-cards-info__title">Total Lumajang</p>
								</div>
							</article>
						</div>
					</div><br>

					<div class="col-md-12 col-xl-12">
						<article class="stat-cards-item">
							<div id="chartContainer" style="height: 370px; width: 100%;"></div>

						</article>
					</div>
				</div>
			</main>
			<!-- ! Footer -->
			<footer class="footer">
				<div class="container footer--flex">
					<div class="footer-start">
						<p>2023</p>
					</div>
				</div>
			</footer>
		</div>
	</div>
	<script src="assets/plugins/chart.min.js"></script>
	<script src="assets/plugins/feather.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
	<script>
		window.onload = function() {
			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				title: {
					text: "Presentas Wilayah"
				},	
				data: [{
					type: "pie",
					yValueFormatString: "#,##0\"%\"",
					indexLabel: "{label} ({y})",
					dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
				}]
			});
			chart.render();

		}
	</script>
</body>

</html>