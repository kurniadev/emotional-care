<?php
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../login.php");
}
include '../config/koneksi.php';
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
$username = strtoupper($peg['username']);
$fstchar = mb_substr($username, 0, 1);
$sideName = "condition-data";

// Get Count Umum (type = result)
$queryUmum = "SELECT COUNT(*) as count FROM evaluation WHERE result ";
$resultUmum = mysqli_query($koneksi, $queryUmum);
$rowUmum = mysqli_fetch_assoc($resultUmum);
$umumCount = $rowUmum['count'];

// Get Count Result 0
$queryResult0 = "SELECT COUNT(*) as count FROM evaluation WHERE result = '0'";
$resultResult0 = mysqli_query($koneksi, $queryResult0);
$rowResult0 = mysqli_fetch_assoc($resultResult0);
$result0Count = $rowResult0['count'];

// Get Count Result 1
$queryResult1 = "SELECT COUNT(*) as count FROM evaluation WHERE result = '1'";
$resultResult1 = mysqli_query($koneksi, $queryResult1);
$rowResult1 = mysqli_fetch_assoc($resultResult1);
$result1Count = $rowResult1['count'];

// Get Count Result 2
$queryResult2 = "SELECT COUNT(*) as count FROM evaluation WHERE result = '2'";
$resultResult2 = mysqli_query($koneksi, $queryResult2);
$rowResult2 = mysqli_fetch_assoc($resultResult2);
$result2Count = $rowResult2['count'];


$dataPoints = array(
    array("y" => $result0Count, "label" => "Normal"),
    array("y" => $result1Count, "label" => "Borderline"),
    array("y" => $result2Count, "label" => "Abnormal"),
);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="shortcut icon" href="assets/img/svg/logo.svg" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/style.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
    .pagination {
        margin-top: 20px;
        text-align: center;

    }

    .pagination a {
        display: inline-block;
        padding: 8px 16px;
        text-decoration: none;
        color: #333;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        margin-right: 4px;
    }

    .pagination a.active {
        background-color: #33a474;
        color: #fff;
    }

    .pagination a:hover {
        background-color: #237050;
        color: #fff;
    }
    .btn {
    display: inline-block;
    /* text-align: center; */
    text-decoration: none;
    font-weight: bold;
    border-radius: 5px;
    padding: 8px 20px;
    color: #ffffff;
    background-color: #007bff;
    border: none;
    cursor: pointer;
    transition: background-color 0.3s ease;
  }

  .btn:hover {
    background-color: #0056b3;
  }

  .btn-block {
    display: block;
    width: 50%;
  }
</style>

<body>
    <div class="layer"></div>
    <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
    <div class="page-flex">
        <?php include 'component/sidebar.php'; ?>
        <div class="main-wrapper">
            <?php include 'component/main-navbar.php'; ?>
            <main class="main users chart-page" id="skip-target">
                <div class="container">
                    <h2 class="main-title">Data Kondisi</h2>
                    <div class="row stat-cards">
                        <div class="col-md-4 col-xl-4">
                            <article class="stat-cards-item">
                                <div class="stat-cards-icon primary">
                                    <i class="fa-solid fa-calendar-check" style="font-size: 30px; display: inline-block; text-align: center;"></i>
                                </div>
                                <div class="stat-cards-info">
                                    <p class="stat-cards-info__num"><?php echo $result0Count; ?> Orang</p>
                                    <p class="stat-cards-info__title">Total Normal</p>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-xl-4">
                            <article class="stat-cards-item">
                                <div class="stat-cards-icon primary">
                                    <i class="fa-solid fa-calendar-check" style="font-size: 30px; display: inline-block; text-align: center;"></i>
                                </div>
                                <div class="stat-cards-info">
                                    <p class="stat-cards-info__num"><?php echo $result1Count; ?> Orang</p>
                                    <p class="stat-cards-info__title">Total Borderline</p>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-xl-4">
                            <article class="stat-cards-item">
                                <div class="stat-cards-icon primary">
                                    <i class="fa-solid fa-calendar-check" style="font-size: 30px; display: inline-block; text-align: center;"></i>
                                </div>
                                <div class="stat-cards-info">
                                    <p class="stat-cards-info__num"><?php echo $result2Count; ?> Orang</p>
                                    <p class="stat-cards-info__title">Total Abnormal</p>
                                </div>
                            </article>
                        </div>
                    </div><br>

                    <div class="col-md-12 col-xl-12">
                        <article class="stat-cards-item">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>

                        </article>
                    </div><br>
                    <!-- <div class="row"> -->
                    <div class="col-md-12 col-xl-12">
                        <div class="users-table table-wrapper">
                            <table class="posts-table">
                                <thead>
                                    <tr class="users-table-info">
                                        <th style="width: 5%; text-align: center;">No.</th>
                                        <th style="width: 10%; ">Tanggal</th >
                                        <th >Nama</th>
                                        <th >Hasil</th>
                                        <th style="width: 20%; text-align: center;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $records_per_page = 10; // Jumlah data per halaman
                                    $current_page = isset($_GET['page']) ? $_GET['page'] : 1; // Halaman saat ini

                                    // Query untuk mengambil jumlah total data
                                    $query_total = "SELECT COUNT(*) as total FROM evaluation";
                                    $result_total = mysqli_query($koneksi, $query_total);
                                    $row_total = mysqli_fetch_assoc($result_total);
                                    $total_records = $row_total['total'];

                                    // Menghitung total halaman
                                    $total_pages = ceil($total_records / $records_per_page);

                                    // Menghitung offset untuk query
                                    $offset = ($current_page - 1) * $records_per_page;

                                    // Query untuk mengambil data dengan paginasi
                                    $query = "SELECT * FROM evaluation
                                            JOIN guest ON guest.id_user = evaluation.id_user
                                            ORDER BY evaluation.date DESC LIMIT $offset, $records_per_page";
                                    $result = mysqli_query($koneksi, $query);

                                    if (!$result) {
                                        die("Query Error: " . mysqli_errno($koneksi) . " - " . mysqli_error($koneksi));
                                    }
                                    $no = $offset + 1; // Nomor urut awal untuk halaman saat ini

                                    while ($row = mysqli_fetch_assoc($result)) {

                                    ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $no; ?></td>
                                            <td ><?php echo date("d-M-Y", strtotime($row['date'])); ?></td>
                                            <td>    <?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?></td>
                                            <td><?php
                                                if ($row['result'] == 0) {
                                                    echo 'Normal';
                                                } else if ($row['result'] == 1) {
                                                    echo 'Borderline';
                                                } else {
                                                    echo 'Abnormal';
                                                } ?></td>
                                            <td style="width: 20%;" >
                                                <center>
                                                    <a href="detail.php?id=<?php echo $row['id_evaluation']; ?>" type="button" class="btn btn-primary btn-block" style="height: 35px;">Detail</a>
                                                </center>
                                            </td>
                                        </tr>
                                    <?php
                                        $no++; //untuk nomor urut terus bertambah 1
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div class="pagination">
                                <?php
                                // Tampilkan tombol halaman sebelumnya jika bukan halaman pertama
                                if ($current_page > 1) {
                                    echo '<a href="?page=' . ($current_page - 1) . '">&laquo;</a>';
                                }

                                // Tampilkan tombol untuk setiap halaman
                                for ($i = 1; $i <= $total_pages; $i++) {
                                    echo '<a href="?page=' . $i . '"';
                                    if ($i == $current_page) {
                                        echo ' class="active"';
                                    }
                                    echo '>' . $i . '</a>';
                                }

                                // Tampilkan tombol halaman selanjutnya jika bukan halaman terakhir
                                if ($current_page < $total_pages) {
                                    echo '<a href="?page=' . ($current_page + 1) . '">&raquo;</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- </div> -->
                </div>
            </main>
            <!-- ! Footer -->
            <footer class="footer">
                <div class="container footer--flex">
                    <div class="footer-start">
                        <p>2023</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="assets/plugins/chart.min.js"></script>
    <script src="assets/plugins/feather.min.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title: {
                    text: "Kondisi"
                },
                axisY: {
                    title: "Total"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## Responden",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
</body>

</html>