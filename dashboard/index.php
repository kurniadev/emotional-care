<?php
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
	header("Location: ../login.php");
}
include '../config/koneksi.php';
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
$username = strtoupper($peg['username']);
$fstchar = mb_substr($username, 0, 1);
$sideName = "index";
// echo "$sideName";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard</title>
	<link rel="shortcut icon" href="assets/img/svg/logo.svg" type="image/x-icon">
	<link rel="stylesheet" href="assets/css/style.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
	<div class="layer"></div>
	<a class="skip-link sr-only" href="#skip-target">Skip to content</a>
	<div class="page-flex">
		<?php include 'component/sidebar.php'; ?>
		<div class="main-wrapper">
		<?php include 'component/main-navbar.php'; ?>
			<main class="main users chart-page" id="skip-target">
				<div class="container">
					<h2 class="main-title">Dashboard</h2>
					<div class="row stat-cards">
						<div class="col-md-6 col-xl-3">
							<article class="stat-cards-item">
								<div class="stat-cards-icon primary">
									<i data-feather="bar-chart-2" aria-hidden="true"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num"><i class="fas fa-link"></i>1478 286</p>
									<p class="stat-cards-info__title">Total Pengunjung</p>
								</div>
							</article>
						</div>
						<div class="col-md-6 col-xl-3">
							<article class="stat-cards-item">
								<div class="stat-cards-icon warning">
									<i data-feather="file" aria-hidden="true"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num">1478 286</p>
									<p class="stat-cards-info__title">Total Tamu</p>
								</div>
							</article>
						</div>
						<div class="col-md-6 col-xl-3">
							<article class="stat-cards-item">
								<div class="stat-cards-icon purple">
									<i data-feather="file" aria-hidden="true"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num">1478 286</p>
									<p class="stat-cards-info__title">Total visits</p>
								</div>
							</article>
						</div>
						<div class="col-md-6 col-xl-3">
							<article class="stat-cards-item">
								<div class="stat-cards-icon success">
									<i data-feather="feather" aria-hidden="true"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num">1478 286</p>
									<p class="stat-cards-info__title">Total visits</p>
								</div>
							</article>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="chart">
								<canvas id="myChart" aria-label="Site statistics" role="img"></canvas>
							</div>
							<!-- <div class="users-table table-wrapper">
								<table class="posts-table">
									<thead>
										<tr class="users-table-info">
											<th>
												<label class="users-table ms-20">
													No.
												</label>
											</th>
											<th>Title</th>
											<th>Author</th>
											<th>Status</th>
											<th>Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												1.
											</td>
											<td>
												Starting your traveling blog with Vasco
											</td>
											<td>
												<div class="pages-table-img">
													<picture>
														<source srcset="assets/img/avatar/avatar-face-04.webp" type="image/webp"><img src="assets/img/avatar/avatar-face-04.png" alt="User Name">
													</picture>
													Jenny Wilson
												</div>
											</td>
											<td><span class="badge-pending">Pending</span></td>
											<td>17.04.2021</td>
											<td>
												<span class="p-relative">
													<button class="dropdown-btn transparent-btn" type="button" title="More info">
														<div class="sr-only">More info</div>
														<i data-feather="more-horizontal" aria-hidden="true"></i>
													</button>
													<ul class="users-item-dropdown dropdown">
														<li><a href="##">Edit</a></li>
														<li><a href="##">Quick edit</a></li>
														<li><a href="##">Trash</a></li>
													</ul>
												</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div> -->
						</div>
					</div>
				</div>
			</main>
			<!-- ! Footer -->
			<footer class="footer">
				<div class="container footer--flex">
					<div class="footer-start">
						<p>2023</p>
					</div>
				</div>
			</footer>
		</div>
	</div>
	<!-- Chart library -->
	<script src="assets/plugins/chart.min.js"></script>
	<!-- Icons library -->
	<script src="assets/plugins/feather.min.js"></script>
	<!-- Custom scripts -->
	<script src="assets/js/script.js"></script>
</body>

</html>