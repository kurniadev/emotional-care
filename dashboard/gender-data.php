<?php
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
	header("Location: ../login.php");
}
include '../config/koneksi.php';
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
$username = strtoupper($peg['username']);
$fstchar = mb_substr($username, 0, 1);
$sideName = "gender-data";

// Get Count Man
$queryMan = "SELECT COUNT(*) as count FROM guest WHERE gender = 'Laki-Laki'";
$resultMan = mysqli_query($koneksi, $queryMan);
$rowMan = mysqli_fetch_assoc($resultMan);
$manCount = $rowMan['count'];

// Get Count Women
$queryWomen = "SELECT COUNT(*) as count FROM guest WHERE gender = 'Perempuan'";
$resultWomen = mysqli_query($koneksi, $queryWomen);
$rowWomen = mysqli_fetch_assoc($resultWomen);
$womenCount = $rowWomen['count'];

// Calculate percentage
$totalCount = $manCount + $womenCount;
$manPercentage = ($manCount / $totalCount) * 100;
$womenPercentage = ($womenCount / $totalCount) * 100;

$dataPoints = array(
	array("label" => "Laki-Laki", "y" => $manPercentage),
	array("label" => "Perempuan", "y" => $womenPercentage),
);

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard</title>
	<link rel="shortcut icon" href="assets/img/svg/logo.svg" type="image/x-icon">
	<link rel="stylesheet" href="assets/css/style.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
	<div class="layer"></div>
	<a class="skip-link sr-only" href="#skip-target">Skip to content</a>
	<div class="page-flex">
		<?php include 'component/sidebar.php'; ?>
		<div class="main-wrapper">
			<?php include 'component/main-navbar.php'; ?>
			<main class="main users chart-page" id="skip-target">
				<div class="container">
					<h2 class="main-title">Data Jenis Kelamin</h2>
					<div class="row stat-cards">
						<div class="col-md-6 col-xl-6">
							<article class="stat-cards-item">
								<div class="stat-cards-icon primary">
									<i class="fa-sharp fa-solid fa-person" style="font-size: 30px; display: inline-block; text-align: center;"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num"><?php echo $manCount; ?> Orang</p>
									<p class="stat-cards-info__title">Total Laki-laki</p>
								</div>
							</article>
						</div>
						<div class="col-md-6 col-xl-6">
							<article class="stat-cards-item">
								<div class="stat-cards-icon primary">
									<i class="fa-sharp fa-solid fa-person" style="font-size: 30px; display: inline-block; text-align: center;"></i>
								</div>
								<div class="stat-cards-info">
									<p class="stat-cards-info__num"><?php echo $womenCount; ?> Orang</p>
									<p class="stat-cards-info__title">Total Perempuan</p>
								</div>
							</article>
						</div>
					</div><br>

					<div class="col-md-12 col-xl-12">
						<article class="stat-cards-item">
							<div id="chartContainer" style="height: 370px; width: 100%;"></div>

						</article>
					</div>
				</div>
			</main>
			<!-- ! Footer -->
			<footer class="footer">
				<div class="container footer--flex">
					<div class="footer-start">
						<p>2023</p>
					</div>
				</div>
			</footer>
		</div>
	</div>
	<script src="assets/plugins/chart.min.js"></script>
	<script src="assets/plugins/feather.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
	<script>
		window.onload = function() {
			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				title: {
					text: "Presentas Jenis Kelamin"
				},	
				data: [{
					type: "doughnut",
					yValueFormatString: "#,##0\"%\"",
					indexLabel: "{label} ({y})",
					dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
				}]
			});
			chart.render();

		}
	</script>
</body>

</html>