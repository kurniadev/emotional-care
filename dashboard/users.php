<?php
ob_start();
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: ../login.php");
}
include '../config/koneksi.php';
$tampilPeg    = mysqli_query($koneksi, "SELECT * FROM users WHERE email='$_SESSION[email]'");
$peg    = mysqli_fetch_array($tampilPeg);
$username = strtoupper($peg['username']);
$fstchar = mb_substr($username, 0, 1);
$sideName = basename($_SERVER['PHP_SELF'], ".php");
// echo "$sideName";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/svg/logo.svg" type="image/x-icon">
    <!-- Custom styles -->
    <link rel="stylesheet" href="assets/css/style.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .pagination {
            margin-top: 20px;
            text-align: center;

        }

        .pagination a {
            display: inline-block;
            padding: 8px 16px;
            text-decoration: none;
            color: #333;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            margin-right: 4px;
        }

        .pagination a.active {
            background-color: #33a474;
            color: #fff;
        }

        .pagination a:hover {
            background-color: #237050;
            color: #fff;
        }
    </style>
</head>

<body>
    <div class="layer"></div>
    <!-- ! Body -->
    <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
    <div class="page-flex">
        <!-- ! Sidebar -->
        <?php include 'component/sidebar.php'; ?>
        <div class="main-wrapper">
            <!-- ! Main nav -->
            <?php include 'component/main-navbar.php'; ?>
            <!-- ! Main -->
            <main class="main users chart-page" id="skip-target">
                <div class="container">
                    <h2 class="main-title">User</h2>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="users-table table-wrapper">
                                <table class="posts-table">
                                    <thead>
                                        <tr class="users-table-info">
                                            <th style="width : 60px">
                                                <label class="users-table ms-20">
                                                    No.
                                                </label>
                                            </th>
                                            <th>Gambar</th>
                                            <th>Nama Lengkap</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Umur</th>
                                            <th>Wilayah</th>
                                            <th>Kategori</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $records_per_page = 1; // Jumlah data per halaman
                                        $current_page = isset($_GET['page']) ? $_GET['page'] : 1; // Halaman saat ini

                                        // Query untuk mengambil jumlah total data
                                        $query_total = "SELECT COUNT(*) as total FROM guest";
                                        $result_total = mysqli_query($koneksi, $query_total);
                                        $row_total = mysqli_fetch_assoc($result_total);
                                        $total_records = $row_total['total'];

                                        // Menghitung total halaman
                                        $total_pages = ceil($total_records / $records_per_page);

                                        // Menghitung offset untuk query
                                        $offset = ($current_page - 1) * $records_per_page;

                                        // Query untuk mengambil data dengan paginasi
                                        $query = "SELECT * FROM guest ORDER BY created_at DESC LIMIT $offset, $records_per_page";
                                        $result = mysqli_query($koneksi, $query);

                                        if (!$result) {
                                            die("Query Error: " . mysqli_errno($koneksi) . " - " . mysqli_error($koneksi));
                                        }
                                        $no = $offset + 1; // Nomor urut awal untuk halaman saat ini

                                        while ($row = mysqli_fetch_assoc($result)) {

                                        ?>
                                            <tr>
                                                <td><?php echo $no; ?>.</td>
                                                <td>
                                                    <div class="pages-table-img">
                                                        <span class="<?= array_rand(array_flip(['random-user-img1', 'random-user-img2', 'random-user-img3', 'random-user-img4', 'random-user-img5'])) ?>">
                                                            <?php echo mb_substr($row['first_name'], 0, 1); ?> <?php echo mb_substr($row['last_name'], 0, 1); ?>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?>
                                                </td>
                                                <td><?php echo $row['gender']; ?></td>
                                                <td><?php echo $row['age']; ?></td>
                                                <td><?php echo $row['region']; ?></td>
                                                <td><?php echo $row['type']; ?></td>
                                                <td>
                                                    <span class="p-relative">
                                                        <button class="dropdown-btn transparent-btn" type="button" title="More info">
                                                            <div class="sr-only">Laki-laki</div>
                                                            <i data-feather="more-horizontal" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class="users-item-dropdown dropdown">
                                                            <li><a href="##">Edit</a></li>
                                                            <li><a href="##">Quick edit</a></li>
                                                            <li><a href="##">Trash</a></li>
                                                        </ul>
                                                    </span>
                                                </td>
                                            </tr>
                                        <?php
                                            $no++; //untuk nomor urut terus bertambah 1
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <div class="pagination">
                                    <?php
                                    // Tampilkan tombol halaman sebelumnya jika bukan halaman pertama
                                    if ($current_page > 1) {
                                        echo '<a href="?page=' . ($current_page - 1) . '">&laquo;</a>';
                                    }

                                    // Tampilkan tombol untuk setiap halaman
                                    for ($i = 1; $i <= $total_pages; $i++) {
                                        echo '<a href="?page=' . $i . '"';
                                        if ($i == $current_page) {
                                            echo ' class="active"';
                                        }
                                        echo '>' . $i . '</a>';
                                    }

                                    // Tampilkan tombol halaman selanjutnya jika bukan halaman terakhir
                                    if ($current_page < $total_pages) {
                                        echo '<a href="?page=' . ($current_page + 1) . '">&raquo;</a>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="container">
                    <h2 class="main-title">Users</h2>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="users-table table-wrapper">
                                <table class="posts-table">
                                    <thead>
                                        <tr class="users-table-info">
                                            <th>
                                                <label class="users-table ms-20">
                                                    No.
                                                </label>
                                            </th>
                                            <th>Title</th>
                                            <th>Author</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
                                        $query = "SELECT * FROM question ORDER BY created_at DESC ";
                                        $result = mysqli_query($koneksi, $query);

                                        //mengecek apakah ada error ketika menjalankan query
                                        if (!$result) {
                                            die("Query Error: " . mysqli_errno($koneksi) .
                                                " - " . mysqli_error($koneksi));
                                        }

                                        //buat perulangan untuk element tabel dari data mahasiswa
                                        $no = 1; //variabel untuk membuat nomor urut
                                        // hasil query akan disimpan dalam variabel $data dalam bentuk array
                                        // kemudian dicetak dengan perulangan while
                                        while ($row = mysqli_fetch_assoc($result)) {

                                        ?>
                                            <tr>
                                                <td><?php echo $no; ?>.</td>
                                                <td>
                                                    <div class="pages-table-img">
                                                        <span class="<?= array_rand(array_flip(['random-user-img1', 'random-user-img2', 'random-user-img3', 'random-user-img4', 'random-user-img5'])) ?>">
                                                        <?php echo mb_substr($row['name_question'], 0, 1); ?>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    Starting your traveling blog with Vasco
                                                </td>
                                                <td><span class="badge-pending">Pending</span></td>
                                                <td>17.04.2021</td>
                                                <td>
                                                    <span class="p-relative">
                                                        <button class="dropdown-btn transparent-btn" type="button" title="More info">
                                                            <div class="sr-only">More info</div>
                                                            <i data-feather="more-horizontal" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class="users-item-dropdown dropdown">
                                                            <li><a href="##">Edit</a></li>
                                                            <li><a href="##">Quick edit</a></li>
                                                            <li><a href="##">Trash</a></li>
                                                        </ul>
                                                    </span>
                                                </td>
                                            </tr>
                                        <?php
                                            $no++; //untuk nomor urut terus bertambah 1
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> -->
            </main>
            <!-- ! Footer -->
            <footer class="footer">
                <div class="container footer--flex">
                    <div class="footer-start">
                        <p>2023</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- Chart library -->
    <script src="assets/plugins/chart.min.js"></script>
    <!-- Icons library -->
    <script src="assets/plugins/feather.min.js"></script>
    <!-- Custom scripts -->
    <script src="assets/js/script.js"></script>
</body>

</html>